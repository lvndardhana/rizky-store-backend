-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Nov 2022 pada 14.08
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categorys`
--

CREATE TABLE `categorys` (
  `id_category` int(10) NOT NULL,
  `category` varchar(30) NOT NULL,
  `create_at` bigint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categorys`
--

INSERT INTO `categorys` (`id_category`, `category`, `create_at`) VALUES
(26, 'Kebutuhan Memasak', 1635568313059),
(29, 'Minuman', 1635583995393);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id_product` int(10) NOT NULL,
  `name_product` varchar(30) DEFAULT NULL,
  `price_product` int(20) DEFAULT NULL,
  `stock_product` int(20) DEFAULT NULL,
  `image_product` varchar(100) DEFAULT NULL,
  `category_product` varchar(30) DEFAULT NULL,
  `last_modified` bigint(30) DEFAULT NULL,
  `stock_unit` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id_product`, `name_product`, `price_product`, `stock_product`, `image_product`, `category_product`, `last_modified`, `stock_unit`) VALUES
(76, 'Aqua 600 ml', 4000, 50, 'http://localhost:9003/public/product-1669207822571.jpg', 'Minuman', 1669208239624, 'pcs'),
(78, 'Bimoli 1L', 15000, 20, 'http://localhost:9003/public/product-1669207909017.jpg', 'Kebutuhan Memasak', 1669207909042, 'pcs');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `username` varchar(30) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fullname` varchar(30) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `token_create_at` bigint(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`username`, `password`, `fullname`, `token`, `token_create_at`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Loven', 'rf70bzyu0lyxq0ls125kcczg8t7f3x1g3iggsz6qgp4pni9aw5eu0hpdd7iy21pi4cin1mumistwr67qitxwk2rfgrzgbbtj6oy6', 1669207769114);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id_category`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id_category` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
